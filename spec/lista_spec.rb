require 'spec_helper'
require 'lista'

describe Lista do

  LIBRO1 = Libro::Libro.new()

    aut0=%w{'Dave Thomas' 'Andy Hunt' 'Chad Fowler'}
      Tit0 = "Programming Ruby 1.9 & 2.0: The Pragmatic Programmers’ Guide"
      Ser0 = "(The Facets of Ruby)"
      Edt0 = "Pragmatic Bookshelf"
      Edc0 = "4 edition"
      fch0 = "(July 7, 2013)"
      num0 = %w{ ISBN-13:978-1937785499 ISBN-10:1937785491 }
      
      LIBRO1.setA(aut0)
      LIBRO1.setT(Tit0)
      LIBRO1.setS(Ser0)
      LIBRO1.setEdt(Edt0)
      LIBRO1.setEdc(Edc0)
      LIBRO1.setFecha(fch0)
      LIBRO1.setNum(num0)

describe " # NODO " do
  it 'Existe nodo' do
    
          
      
        L1 = Lista::Lista.new()
        L1.insert(LIBRO1)   ##hay que insertar para que el nodo no sea nulo
        expect(L1.nodo_ini).to_not be_nil
        
  end
end

describe " # LISTA" do
  
  it 'Se puede insertar un elemento' do
    
    #Libro2 = Libro::Libro.new()
        L2 = Lista::Lista.new()
        L2.insert(LIBRO1)   
        expect(L2.nodo_ini).to_not be_nil
    
  end
  
  it 'Se pueden insertar varios elementos' do
   # Libro3 = Libro::Libro.new()
        L3 = Lista::Lista.new()
        L3.insert(LIBRO1) 
        L3.insert(LIBRO1)   

        expect(L3.nodo_ini).to_not be_nil
        expect(L3.nodo_act).to_not be_nil

  end
  
  it 'Existe cabeza' do
   # Libro4 = Libro::Libro.new()
        L4 = Lista::Lista.new()
        L4.insert(LIBRO1)   
        expect(L4.cabeza).to eq(L4.nodo_act)
  end
  
  it 'Se extrae el primer elmento de la lista' do 
   
   # Libro5 = Libro::Libro.new()
  #  Libro6 = Libro::Libro.new()

        L5 = Lista::Lista.new()
        L5.insert(LIBRO1)
        L5.insert(LIBRO1)

        nodo_auxiliar = L5.nodo_ini
      #  L5.mostrar
      #  puts "\n"
        L5.extraer
      #  puts "\n"
      #  L5.mostrar
      #  puts "\n"

        expect(L5.nodo_ini).to eq(nodo_auxiliar[1])
    
    
  end
  
  
end

describe "# Lista enlazada ambos sentidos" do
    
    it 'Se insertan números y se comprueban los anteriores y siguientes' do
       
        L6 = Lista::Lista.new()
        L6.insert(1)
        L6.insert(2)
        L6.insert(3)
      #  L6.mostrar
        expect(L6.nodo_act[2].v).to eq(2)
        aux=L6.nodo_act[2]
       # puts aux
        expect(aux[1].v).to eq(3)
      #  L6.mostrar
        
        
    end
    
end

describe "# Jerarquía y herencia de la clase libro" do
 r1=Libro::Revista.new("NINTENDO")
    r1.setT("Título 1")
    
    p1=Libro::Periodico.new("Col. 6")
    p1.setT("Periodico 1")
    
    d1=Libro::Documento.new("www.google.es")
    d1.setT("Documento 1")
it 'Creamos los objetos ' do
   
    
    r1.formatea()
    p1.formatea()
    d1.formatea()
    
    expect(r1).to be_an_instance_of(Libro::Revista)
    expect(p1).to be_an_instance_of(Libro::Periodico)
    expect(d1).to be_an_instance_of(Libro::Documento)
    
end
 
 it 'Comprobamos que el padre es Libro' do
            expect(r1).to be_a_kind_of(Libro::Libro)
            expect(p1).to be_a_kind_of(Libro::Libro)
            expect(d1).to be_a_kind_of(Libro::Libro)
        end
        
        it 'Comprobamos de nuevo hijos Libro' do
            expect(r1.nombrerevista).to eq("NINTENDO")
            expect(p1.articulo).to eq("Col. 6")
            expect(d1.url).to eq("www.google.es")
            
        end
    
    
        it 'comprobando tipo' do
            expect(r1.respond_to?:getISBN).to eq(true)
        end
end
    end






